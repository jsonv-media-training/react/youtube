import React from "react";
import { YouTubeVideo } from "../../models/YouTubeVideo.model";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Skeleton from '@material-ui/lab/Skeleton';

interface VideoDetailProps {
  video: YouTubeVideo | null
}

class VideoDetail extends React.Component<VideoDetailProps, any> {

  private get video() {
    return this.props.video;
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <Card>
        {
          !this.video ?
            (<Skeleton style={{ minHeight: 500}} animation="wave" variant="rect"/>) :
            (<iframe
              style={{ minHeight: 500}}
              title="something"
              width="100%"
              src={`https://www.youtube-nocookie.com/embed/${this.video.id.videoId}`}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen />)
        }
        <CardContent>
          {
            !this.video ?
              (<React.Fragment>
                  <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                  <Skeleton animation="wave" height={10} width="80%" />
                </React.Fragment>) :
              (this.video.snippet.description)
          }
        </CardContent>
      </Card>
    );
  }
}

export default VideoDetail;
