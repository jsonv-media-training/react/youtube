import React from "react";
import { YouTubeVideo } from "../../models/YouTubeVideo.model";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import "./VideoItem.scss";

interface VideoItemProps {
  video: YouTubeVideo,
  onSelectedVideoFunc: (video: YouTubeVideo) => void
}

class VideoItem extends React.Component<VideoItemProps, any> {

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <ListItem divider button={true} disableGutters={true}
                onClick={() => this.props.onSelectedVideoFunc(this.video) }>
        <ListItemAvatar>
          <Avatar
            variant="square"
            style={{height: this.thumbnail.default.height, width: this.thumbnail.default.width }}
            src={this.thumbnail.medium.url}/>
        </ListItemAvatar>
        <ListItemText primary={this.video.snippet.title}/>
      </ListItem>
    );
  }

  private get video() {
    return this.props.video;
  }

  private get thumbnail() {
    return this.props.video.snippet.thumbnails;
  }
}

export default VideoItem;
