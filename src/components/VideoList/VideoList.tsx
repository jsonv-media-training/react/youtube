import React from "react";
import { YouTubeVideo } from "../../models/YouTubeVideo.model";
import List from "@material-ui/core/List";
import VideoItem from "../VideoItem/VideoItem";

interface VideoListProps {
  videos: YouTubeVideo[],
  onSelectedVideoFunc: (video: YouTubeVideo) => void
}

class VideoList extends React.Component<VideoListProps, any> {

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <List>
        {this.videos}
      </List>
    );
  }

  private get videos() {
    return this.props.videos.map(video => {
      return (
        <VideoItem
          key={video.id.videoId}
          video={video}
          onSelectedVideoFunc={this.props.onSelectedVideoFunc} />
      )
    });
  }
}

export default VideoList;
