import React, { FormEvent, SyntheticEvent } from "react";
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";


interface SearchBarProps {
  onSearchFunc: (key: string) => void
}

interface SearchBarState {
  searchKey: string
}

class SearchBar extends React.Component<SearchBarProps, SearchBarState> {

  state: SearchBarState = { searchKey: '' };

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <form onSubmit={this.onSubmitTerm}>
        <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Search</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={this.state.searchKey}
            onChange={this.onInputChange}
            placeholder="Please type something to search ..."
            type="search"
            startAdornment={<InputAdornment position="start">
              <YoutubeSearchedForIcon fontSize="large"/>
            </InputAdornment>}
            labelWidth={60}
          />
        </FormControl>
      </form>
    );
  }

  private onInputChange = (event: SyntheticEvent) => {
    this.setState({ searchKey: (event.target as HTMLInputElement).value });
  };

  private onSubmitTerm = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.props.onSearchFunc(this.state.searchKey);
  };
}

export default SearchBar;
