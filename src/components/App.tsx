import React from 'react';
import './App.scss';
import Container from "@material-ui/core/Container";
import SearchBar from "./SearchBar/SearchBar";
import { search } from "../services/YouTubeAPI.service";
import { YouTubeVideo } from "../models/YouTubeVideo.model";
import VideoList from "./VideoList/VideoList";
import VideoDetail from "./VideoDetail/VideoDetail";
import Grid from '@material-ui/core/Grid';

interface AppState {
  selectedVideo: YouTubeVideo | null,
  videos: YouTubeVideo[]
}

class App extends React.Component<any, AppState> {

  state = { selectedVideo: null, videos: []};

  private onSearch = async (key: string) => {
    const { data } = await search(key);
    this.setState({ selectedVideo: data.items[0], videos: data.items })
  };

  private onSelectedVideo = (video: YouTubeVideo) => {
    this.setState({ selectedVideo: video });
  };

  async componentDidMount() {
    await this.onSearch('Messi');
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
      <Container maxWidth="lg" style={{marginTop: "25px"}}>
        <SearchBar onSearchFunc={this.onSearch}/>
        <br/>
        <Grid container spacing={2}>
          <Grid item md={7}>
            <VideoDetail video={this.state.selectedVideo}/>
          </Grid>
          <Grid item md={5}>
            <VideoList onSelectedVideoFunc={this.onSelectedVideo} videos={this.state.videos}/>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default App;
