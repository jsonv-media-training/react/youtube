import axios from "axios";
import { Res } from "../models/Res";
import { YouTubeVideoList } from "../models/YouTubeVideo.model";

const BASE_URL = 'https://www.googleapis.com/youtube/v3';
const KEY = 'AIzaSyAHipPKIhDV7gDbePyfUaHcD1U0j-_s_zk';

export const search = (searchKey: string): Promise<Res<YouTubeVideoList>> => {
  return axios.get('/search',{
    baseURL: BASE_URL,
    params: {
      q: searchKey,
      part: "snippet",
      maxResults: 5,
      key: KEY
    }
  })
};
