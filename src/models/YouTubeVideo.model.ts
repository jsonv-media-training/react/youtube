export interface YouTubeVideoList {
  kind: string,
  etag: string,
  nextPageToken?: string,
  prevPageToken?: string,
  regionCode: string,
  pageInfo: {
    totalResults: number,
    resultsPerPage: number
  },
  items: YouTubeVideo[]
}

export interface YouTubeVideo {
  kind: string,
  etag: string,
  id: {
    kind: string,
    videoId: string,
    channelId?: string,
    playlistId?: string
  },
  snippet: {
    publishedAt: Date,
    publishTime?: Date,
    channelId: string,
    channelTitle: string,
    title: string,
    description: string,
    liveBroadcastContent: string
    thumbnails: {
      default: ThumbNail,
      high: ThumbNail,
      medium: ThumbNail
    }
  }
}

interface ThumbNail {
  url: string,
  width: number,
  height: number
}
